package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.Apitechuv2Application;
import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Las peticiones que entran al controlador tienen de base la ruta /apitechu/v2
 */
@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public List<ProductModel> getProducts() {
        System.out.println("getProducts");

        return this.productService.findAll();
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("La id del producto a buscar es: " + id);

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

        //se sustituye por el ternario de arriba
        /*if(result.isPresent() == true){
            return new ResponseEntity<>(result.get(), HttpStatus.OK);
        }else{
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }*/

        //return new ResponseEntity<>(new ProductModel(), HttpStatus.OK);
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){
        System.out.println("addProduct");
        System.out.println("La id del producto que se va a crear es: " + product.getId());
        System.out.println("La descripción del producto que se va a crear es: " + product.getDesc());
        System.out.println("El precio del producto que se va a crear es: " + product.getPrice());

        return new ResponseEntity<>(this.productService.add(product), HttpStatus.CREATED);
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar es: " + id);

        boolean deletedProduct = this.productService.deleteById(id);

        return new ResponseEntity<>(
                deletedProduct ? "Producto borrado" : "Producto no encontrado",
                deletedProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateById(@RequestBody ProductModel product, @PathVariable String id){
    //public ResponseEntity<Object> updateById(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("updateById");
        System.out.println("La id en parámetro de la url es: " + id);
        System.out.println("La id del producto a actualizar es: " + product.getId());
        System.out.println("La descripcin del producto a actualizar es: " + product.getDesc());
        System.out.println("El precio del producto a actualizar es: " + product.getPrice());

        Optional<ProductModel> productToUpdate = this.productService.findById(id);

        /*if(productToUpdate.isPresent() == true){
            return new ResponseEntity<>(this.productService.add(product), HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }*/
        if(productToUpdate.isPresent() == true){
            System.out.println("Producto para actualizar encontrado, actualizando");
            this.productService.update(product);
        }
        return new ResponseEntity<>(product,
                productToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);

    }
}
