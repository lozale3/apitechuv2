package com.techu.apitechuv2.services;

import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> getUsers(String orderBy){
        System.out.println("getUsers");

        List<UserModel> result;

        if(orderBy != null){
            System.out.println("Se ha pedido ordenacion");

            result = this.userRepository.findAll(Sort.by("age"));
        }else{
            result = this.userRepository.findAll();
        }
        return result;
    }

    public Optional<UserModel> findById(String id){
        System.out.println("findById");
        System.out.println("Obteniendo el usuario con la id: " + id);

        return this.userRepository.findById(id);
    }

    public UserModel add(UserModel user){
        System.out.println("add");

        return this.userRepository.insert(user);
    }

    /***
     *
     * La funcion save guarda el usuario que nos envian y, si ya existe un usuario con ese id,
     * lo crea nuevo
     *
     * @param user
     * @return
     */
    public UserModel update(UserModel user){
        System.out.println("update");

        return this.userRepository.save(user);
    }

    public boolean deleteById (String id){
        System.out.println("delete");
        boolean result = false;

        if(this.userRepository.findById(id).isPresent()){
            System.out.println("Usuario encontrado, borrando...");
            this.userRepository.deleteById(id);
            result = true;
        }
        return result;
    }
}
